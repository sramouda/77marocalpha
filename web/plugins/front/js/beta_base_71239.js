/* important JS */

jQuery(document).ready(function() {
    $("body").on("click", ".displayMenu", function(a) {
        a.preventDefault();
        $("body").addClass("pushed");
        $(".displayMenu, .mobileMenuHoverlay, .headerNav").addClass("open")
        $('.mobileMenuHoverlay i').show();
    });
    $("body").on("click", ".mobileMenuHoverlay", function(a) {
        a.preventDefault();
        $("body").removeClass("pushed");
        $(".displayMenu, .mobileMenuHoverlay, .headerNav").removeClass("open");
        $(".headerNav_footer").addClass("hidden")
        $('.mobileMenuHoverlay i').hide();
        
    });
    $("body").on("click", ".showFooterLinks, .hideFooterLinks", function(a) {
        a.preventDefault();
        $(".headerNav_footer").toggleClass("hidden")
    });
    $("body").on("click", ".popin-hideHoverlay", function(a) {
        $(".mobileMenuHoverlay").trigger("click")
    })
});


    $("body").on("click", ".toggleElement", function(o) {
        if ((!($(this).hasClass("active"))) && $(this).is("a") && ($(this).attr("href") == "")) {
            o.preventDefault()
        }
        o.stopPropagation();
        var n = $(this).data("element"),
            n = n.split(" "),
            l = $(this).data("elementhide"),
            m = $(this).data("toggleclass");
        if (l) {
            if ($("." + l).hasClass("visible")) {
                $("." + l).removeClass("visible")
            } else {
                $("." + l).addClass("hidden")
            }
        }
        if (n && n != "") {
            $.each(n, function(p, q) {
                $("." + q).toggleClass("visible")
            })
        }
        if (m) {
            if (m == "searchbar-open") {
                $("#searchbox").toggleClass("custom-small-hidden");
                $("body").toggleClass("pushed");
                $(".searchbar.toggleElement").toggleClass(m)
            } else {
                $(this).toggleClass(m)
            }
        }
    });

