<?php


namespace AppBundle\Extension;

class  convertCamelCaseToHaveSpaces extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('camelToSpace', array($this, 'convertCamelCaseToHaveSpacesFilter')),
        );
    }

    /*
     * Converts camel case string to have spaces
     */
    public function convertCamelCaseToHaveSpacesFilter($camelCaseString)
    {

        $pattern = '/(([A-Z]{1}))/';
        return preg_replace_callback(
            $pattern,
            function ($matches) {return " " .$matches[0];},
            $camelCaseString
        );
    }

    public function getName()
    {
        return 'app_extension';
    }
}