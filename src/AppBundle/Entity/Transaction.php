<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Transaction
 *
 * @ORM\Table(name="7m_transaction")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TransactionRepository")
 */
class Transaction
{
    /*********** Transaction Type ***********/
    const RECHARGE_DE_SOLDE    = 'RECHARGE_DE_SOLDE';
    const ACHAT_DE_RENOUVELLEMENT    = 'ACHAT_DE_RENOUVELLEMENT';
    const ACHAT_TOUS_JOUR_EN_TETE    = 'ACHAT_TOUS_JOUR_EN_TETE';

   /*********** Payment By ***********/
    const PAYMENT_PAR_CARTE   = 'PAYMENT_PAR_CARTE';
    const PAYMENT_PAR_SOLDE    = 'PAYMENT_PAR_SOLDE';

    /*********** Transaction Status ***********/
    const STATUS_DONE = 'PASSER';
    const STATUS_NOTDONE = 'NON PASSER';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255,nullable=true)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="paymentby", type="string", length=255,nullable=true)
     */
    private $paymentby;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=255,nullable=true)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="total", type="decimal", precision=10, scale=0)
     */
    private $total;

    /**
     * @var int
     *
     * @ORM\Column(name="idad", type="string",nullable=true)
     */
    private $idad;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetimetz", options={"default"="CURRENT_TIMESTAMP"})
     */
    private $createdAt;


    /**
     * @var string
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetimetz", options={"default"="CURRENT_TIMESTAMP"})
     */
    private $updatedAt;

     /**
     * @var int
     *
     * @ORM\Column(name="reloadedaccount", type="integer",nullable=true)
     */
    private $reloadedaccount;

      /**
     * Many Transaction have One User.
     * @ORM\ManyToOne(targetEntity="User", inversedBy="Transaction")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Transaction
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }


    /**
     * Set paymentby
     *
     * @param string $paymentby
     *
     * @return Transaction
     */
    public function setPaymentby($paymentby)
    {
        $this->paymentby = $paymentby;

        return $this;
    }

    /**
     * Get paymentby
     *
     * @return string
     */
    public function getPaymentby()
    {
        return $this->paymentby;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Transaction
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set total
     *
     * @param string $total
     *
     * @return Transaction
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total
     *
     * @return string
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set idad
     *
     * @param string $idad
     *
     * @return Transaction
     */
    public function setIdad($idad)
    {
        $this->idad = $idad;

        return $this;
    }

    /**
     * Get idad
     *
     * @return string
     */
    public function getIdad()
    {
        return $this->idad;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Transaction
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Transaction
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set reloadedaccount
     *
     * @param integer $reloadedaccount
     *
     * @return Transaction
     */
    public function setReloadedaccount($reloadedaccount)
    {
        $this->reloadedaccount = $reloadedaccount;

        return $this;
    }

    /**
     * Get reloadedaccount
     *
     * @return integer
     */
    public function getReloadedaccount()
    {
        return $this->reloadedaccount;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Transaction
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
