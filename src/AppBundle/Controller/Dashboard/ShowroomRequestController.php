<?php

namespace AppBundle\Controller\Dashboard;

use AppBundle\Entity\ShowroomRequest;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * City controller.
 *
 * @Route("dashboard/showroomrequest")
 */
class ShowroomRequestController extends Controller
{

   /**
     * @Route("/", name="showroomrequest_dashboard_index")
     */
    public function indexAction(Request $request)
    {
     
        $em = $this->getDoctrine()->getManager();
        $requests = $em->getRepository('AppBundle:ShowroomRequest')->findAll();

        return $this->render('Dashboard/ShowroomRequest/ShowroomRequest.html.twig', [
            
            'reqs' => $requests
        ]);
    }
 
    /**
     * @Route("/showroomrequest_delete/{id}",options= {"expose" = true},
     * condition="request.isXmlHttpRequest()",name="showroomrequest_delete_index")
     */
    public function showroomrequestDeleteAction(Request $request, ShowroomRequest $showreq)
    {
        if ($showreq) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($showreq);
            $em->flush();
          
            return new JsonResponse(['success' => true]);
        } else {
            return new JsonResponse(['success' => false]);
        }

    }

     

}
