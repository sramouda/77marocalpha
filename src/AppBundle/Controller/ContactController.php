<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Contact;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/77maroc/contactez-nous")
 */
class ContactController extends Controller
{

    /**
     * @Route("/send",name="contact_us")
     */
    public function contactAction(Request $request)
    {
        $em     = $this->getDoctrine()->getManager();
        $contact = new Contact();
        $contact->setNamecomplet($request->request->get('name'));
        $contact->setEmail($request->request->get('email'));
        $contact->setMessage($request->request->get('message'));
        $em->persist($contact);
        $em->flush();
        $this->addFlash('success','Votre Message a été envoyé avec succés !');
        return $this->redirectToRoute('contact_us_render');

    }
    /**
     * @Route("/",name="contact_us_render")
     */
    public function contactRenderAction()
    {
        return $this->render('Default/contact.html.twig');
    }

}
