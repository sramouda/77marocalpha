<?php

namespace AppBundle\Controller\Front;

use AppBundle\Entity\User;
use AppBundle\Entity\PublicityPosition;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
* @Route("/")
*/
class ShowroomController extends Controller
{
    /**
     * @Route("/boutiques", name="showroompage")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $showroomsList = $em->getRepository('AppBundle:User')->findBy(array("accounttype" => 2));
        
        $paginator = $this->get('knp_paginator');
        $showrooms       = $paginator->paginate(
            $showroomsList,
            $request->query->getInt('page', 1),
            intVal(10)
        );

        return $this->render('Default/Showroom/Showroom.html.twig',array(
              'showrooms' => $showrooms,
        ));
        
    }



    /**
     *  @Route("/{slug}",name="showroomads_index",defaults={"slug"=1}) 
    */
    
    public function showroomadsAction(Request $request,$slug)
    {
       $em = $this->getDoctrine()->getManager();

      if($slug == "sitemap.xml"){
            $urls     = [];
        
        $hostName =$request->server->get('HTTP_HOST');
      
        $urls[]    = array('loc' => $this->generateUrl('homepage'),
                  'changefreq' => 'weekly', 'priority' => '1.0');

        $urls[] = array('loc' => $this->generateUrl('page_qui_nous_somme'),
                  'changefreq' => 'weekly', 'priority'=> '1.0');
        $urls[] = array('loc' => $this->generateUrl('page_aide'),
                  'changefreq'         => 'weekly', 'priority'=> '1.0');
        $urls[] = array('loc' => $this->generateUrl('page_achat_securise'),
                  'changefreq'         => 'weekly', 'priority'=> '1.0');
        $urls[] = array('loc' => $this->generateUrl('page_reglement'),
                   'changefreq'  => 'weekly', 'priority'=> '1.0');
        $urls[] = array('loc' => $this->generateUrl('page_publicite'),
                  'changefreq' => 'weekly', 'priority'=> '1.0');
        $allAd = $em->getRepository('AppBundle:Ad')->findAll();
        
        foreach ($allAd as $ad) {
            if($ad->getStatus() === true){
                $urls[] = array('loc' => $this->generateUrl('detail_ad', array('ville'=> $ad->getVillename(),
                                                              'category' => $ad->getCategory(),
                                                              'slug' => $ad->getSlug(),
                                                           )),
                               'priority' => '0.5');

            }

          }
          
             
      return $this->render('SiteMap/site_map.html.twig',['hostname' => $hostName,'urls' => $urls]);

      }else{

          $usr = $em->getRepository(User::class)->findOneBy(['slug' => $slug]);
          if($usr == null){
           throw new NotFoundHttpException('Sorry not existing!');
            
          }
          $adsList = $em->getRepository('AppBundle:Ad')->findBy(array("user" => $usr,'status' => true));
       
          $publicityContact = $em->getRepository('AppBundle:PublicityPosition')->findOneBy([
            'page'     => PublicityPosition::PAGE_ANNONCE,
            'position' => PublicityPosition::PAGE_ANNONCE_UNDER_CONTACT,
        ]);
        $paginator = $this->get('knp_paginator');
        $ads       = $paginator->paginate(
            $adsList,
            $request->query->getInt('page', 1),
            10
        );
        

        return $this->render('Default/Showroom/ShowroomAds.html.twig',array(
              'ads' => $ads,
              'user' => $usr,
              'publicityContact' => $publicityContact
        ));
      }
        
    }
}
