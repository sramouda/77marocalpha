<?php

namespace AppBundle\Controller\Front;

use AppBundle\Entity\Ad;
use AppBundle\Entity\Category;
use AppBundle\Entity\ImagesAd;
use AppBundle\Entity\Inbox;
use AppBundle\Entity\PublicityPosition;
use AppBundle\Services\AutoDetectedBoiteAnnonce;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @Route("/annonce")
 */
class AdsDetailController extends Controller
{
    public function array_random_assoc($arr, $num = 3)
    {
        $keys = array_keys($arr);
        shuffle($keys);

        $r = array();
        for ($i = 0; $i < $num; $i++) {
            $r[$keys[$i]] = $arr[$keys[$i]];
        }
        return $r;
    }

     /**
     *@Route("/{ville}/{category}/{slug}",name="detail_ad",defaults={"ville"="test","category"=1,"slug"=1},options={ "expose" = true })
     *@ParamConverter("ad", options={"mapping": {"slug": "slug"}})
     */
    public function detailAdIndex(Request $request, Ad $ad)
    {

            $em = $this->getDoctrine()->getManager();

           if($ad->getStatus() == false || $ad->getStatus() == null)
             {
                if(!$this->getUser()){
                     return $this->redirect($this->generateUrl('ads_page'));
                }
                if(!$this->getUser()->getUsername() == $ad->getUser()->getUsername())
                {
                    return $this->redirect($this->generateUrl('ads_page'));
                
                } 
               
             }else{
                  $ad->setNumbervisit($ad->getNumbervisit() + 1);
                  $this->getDoctrine()->getManager()->flush();
             }
                   
      
            
        $inbox = new Inbox();

        $form = $this->createForm('AppBundle\Form\InboxType', $inbox);
        $form->handleRequest($request);
        $listOfAdUser = $ad->getUser()->getAd();
        $listfObjectAd = [];
        foreach ($listOfAdUser as $key => $object) {
            if ($object->getId() == $ad->getId() || $object->getStatus() != true) {
                continue;
             }
             array_push($listfObjectAd, $object);
        }
        if (!empty($listfObjectAd)) {
            if (count($listfObjectAd) > 4) {
                $listfObjectAd = $this->array_random_assoc($listfObjectAd,4);
                
            } else {
                $listfObjectAd = $listfObjectAd;
            }
        }
        if ($form->isSubmitted() && $form->isValid()) {
            $inbox->setType(Inbox::ANNONYME);
            $inbox->setSubject($ad->getTitle());
            $inbox->setUser($ad->getUser());
            $em->persist($inbox);
            $em->flush();
            return $this->redirectToRoute('detail_ad', [ 'ville'    => $ad->getVillename(),
                                                         'category' => $ad->getCategory(),
                                                         'slug' => $ad->getSlug(),
                                                    ]);
        }

        $publicityTop = $em->getRepository('AppBundle:PublicityPosition')->findOneBy([
            'page'     => PublicityPosition::PAGE_ANNONCE,
            'position' => PublicityPosition::PAGE_ANNONCE_UNDER_TITLE,
        ]);
        $publicityContact = $em->getRepository('AppBundle:PublicityPosition')->findOneBy([
            'page'     => PublicityPosition::PAGE_ANNONCE,
            'position' => PublicityPosition::PAGE_ANNONCE_UNDER_CONTACT,
        ]);
        $publicityBottom = $em->getRepository('AppBundle:PublicityPosition')->findOneBy([
            'page'     => PublicityPosition::PAGE_ANNONCE,
            'position' => PublicityPosition::PAGE_ANNONCE_BELOW_ANNONCE,
        ]);

        return $this->render('Default/Ads/adDetail.html.twig', ['ad' => $ad,
            'publicityTop'                                               => $publicityTop,
            'publicityContact'                                           => $publicityContact,
            'publicityBottom'                                            => $publicityBottom,
            "form"                                                       => $form->createView(),
            "listOfLastAd"                                               => $listfObjectAd
        ]);
    }
}