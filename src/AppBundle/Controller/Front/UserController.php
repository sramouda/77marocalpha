<?php

namespace AppBundle\Controller\Front;

use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\GetResponseUserEvent;

/**
 * @Route("/user")
 */

class UserController extends Controller
{
    /**
     * @Route("/account/registration",name="user_registration")
     * @Method({"GET", "POST"})
     */
    public function RegisterAction(Request $request)
    {
        //$dispatcher = $this->get('event_dispatcher');
        $userManager = $this->container->get('fos_user.user_manager');
        $user        = $userManager->createUser();
        //$event = new GetResponseUserEvent($user, $request);
        $formCreateUser = $this->createForm('AppBundle\Form\UserRegistrationType', $user);
        $formCreateUser->handleRequest($request);
        $em = $this->getDoctrine()->getManager();
        $accounttype1 = $em->getRepository('AppBundle:AccountType')->findOneBy(['id' => 1]);
       
        if ($formCreateUser->isSubmitted()) {
            
            $user->setUsername($user->getEmail());
            $user->setRoles(["ROLE_NORMAL"]);
            $user->setAccounttype($accounttype1);
            $user->setEnabled(true);
            //$user->setWaiting_code($this->randomKey());
                 

            $userManager->updateUser($user);
           // $dispatcher->dispatch(FOSUserEvents::REGISTRATION_SUCCESS, $event);

            $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
            $this->container->get('security.token_storage')->setToken($token);
            $this->container->get('session')->set('_security_main', serialize($token));
            
            return $this->redirectToRoute('user_waiting_confirmation');
        }

        return $this->render('Default/User/Register.html.twig', array(
            'form' => $formCreateUser->createView(),
        ));

    }

    /**
     * @Route("/login/normal",name="normal_login_index")
     * @Method({"GET", "POST"})
     */
    public function NormalAction(Request $request)
    {
        $session = new Session();
        if(!$this->container->get('session')->isStarted()){
            $session->start();
        }
        
        $session->set('vitrine', false);
 
        return $this->redirectToRoute('account_login');
    }

     /**
     * @Route("/login/showroom",name="showroom_login_index")
     * @Method({"GET", "POST"})
     */
    public function ShowroomAction(Request $request)
    {
        $session = new Session();
        if(!$this->container->get('session')->isStarted()){
            $session->start();
        }
        $session->set('vitrine', true);
        return $this->redirectToRoute('account_login');
    }

     /**
     * @Route("/thankyou",name="user_waiting_confirmation")
     * @Method({"GET", "POST"})
     */
    public function waitingConfirmationAction(Request $request)
    {
 

        return $this->render('Default/User/Waiting_confirmation.html.twig');

    }


     /**
     * @Route("/redirectionsw",name="go_tohome_confirmationsw")
     */
    public function GoHomeConfirmationAction(Request $request)
    {       
            return $this->redirectToRoute('ads_page');
    
    }
    //** Get Random confirmation code ***/
    function randomKey($length = 8) {
        $key = "";
        $pool = array_merge(range(0,9), range('a', 'z'),range('A', 'Z'));
    
        for($i=0; $i < $length; $i++) {
            $key .= $pool[mt_rand(0, count($pool) - 1)];
        }
        return $key;
    }

  
}