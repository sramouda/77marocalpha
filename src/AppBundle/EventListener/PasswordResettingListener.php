<?php
namespace AppBundle\EventListener;

use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Listener responsible to change the redirection at the end of the password resetting
 */
class PasswordResettingListener implements EventSubscriberInterface
{
    private $router;
    private $securityContext;

    public function __construct(UrlGeneratorInterface $router,ContainerInterface $container)
    {   
        $this->container = $container;
        $this->router = $router;
        $this->securityContext = $container->get('security.token_storage'); 
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return array(
            FOSUserEvents::RESETTING_RESET_SUCCESS => 'onPasswordResettingSuccess',
        );
    }

    public function onPasswordResettingSuccess(FormEvent $event)
    {   
        $user = $this->securityContext->getToken()->getUser();
        
        $url = $this->router->generate('ads_page');

        $event->setResponse(new RedirectResponse($url));
    }
}